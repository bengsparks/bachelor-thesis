debug ?= 1

define version-check =
	@$(eval version_param=$(shell test -n '$3' && echo '$3' || echo --version))
	@$(eval regex=$(shell test -n '$4' && echo '$4' || echo '^.* \K.[.0-9-]+[a-z]?'))
	@test -n "$(debug)" && echo -e "\e[0;36m[debug   ]\e[0m Installed $1 version:" \
		"$(shell $1 $(version_param) 2>&1 | grep -Po -m1 '$(regex)')" || true
	@echo -ne "\e[0;34m[info    ]\e[0m Checking $1: "
	@$(eval version=$(shell $1 $(version_param) 2>&1 | grep -Po -m1 '$(regex)' | xargs echo $2 | tr ' ' '\n' | sort -V | head -1))
	@$(eval version=$(shell which $1 > /dev/null 2>&1 && echo "$(version)" || echo not installed))
	@test "$(version)" == $2 && echo -e "\e[1;32mok\e[0m" || ( \
		echo -e "\e[1;31mfail\e[0m" && \
		echo -e "\e[0;31m[error   ]\e[0m Please install $1 >=$2 to proceed!" \
	)
	@$(eval version_check_failed ?= $(shell test "$(version)" != $2 && echo 1))
endef

THESIS_CSL ?= resources/ieee.csl
THESIS_TEMPLATE ?= resources/template.tex
THESIS_BIBLIOGRAPHY ?= resources/thesis.bib
THESIS_INCLUDE_BEFORE ?= resources/startthesis.tex
THESIS_INCLUDE_AFTER ?= resources/appendix.tex
THESIS_SETTINGS ?= settings.yaml
THESIS_CHAPTERS ?= ./chapters
THESIS_OUTPUT ?= result

SPELL_CHECK_LANGUAGE ?= en_US
SPELL_CHECK_USER_DICT ?= ~/.hunspell_en_US

VERSION_CHECK_LOCK ?= .version-check-passed.lock

LOG_BUILD ?= build
LOG_SPELL_CHECK ?= spell_check

THESIS_FILES := \
	$(THESIS_CSL) $(THESIS_TEMPLATE) \
	$(THESIS_BIBLIOGRAPHY) $(THESIS_INCLUDE_BEFORE) \
	$(THESIS_INCLUDE_AFTER) $(THESIS_SETTING) \
	$(wildcard $(THESIS_CHAPTERS)/*.md)

pandoc-args = \
	--verbose --preserve-tabs --number-sections \
	--from=markdown --csl "$(THESIS_CSL)" \
	--include-before-body="$(THESIS_INCLUDE_BEFORE)" \
	--include-after-body="$(THESIS_INCLUDE_AFTER)" \
	--template="$(THESIS_TEMPLATE)" \
	--output="$(THESIS_OUTPUT).$1" \
	--biblatex \
	--pdf-engine-opt=-shell-escape \
	"$(THESIS_SETTINGS)" "$(THESIS_CHAPTERS)/"*.md

PDFLATEX_ARGS = -shell-escape -interaction=nonstopmode -jobname=intermediate "$(THESIS_OUTPUT).tex"

all: build

build: $(LOG_SPELL_CHECK).log pdf
	@echo -e "\e[0;32m[success ]\e[0m Build succeeded!"

clean:
	@echo -e "\e[0;34m[info    ]\e[0m Cleaning project..."
	@rm -f intermediate.* "$(THESIS_OUTPUT)".* "$(LOG_SPELL_CHECK)".log "$(LOG_BUILD)"_*.log texput.log $(VERSION_CHECK_LOCK) && \
		echo -e "\e[0;32m[success ]\e[0m Clean succeeded!" || (echo -e "\e[0;31m[error   ]\e[0m Clean failed!" >&2 && exit 1)

check: $(VERSION_CHECK_LOCK)

spell-check: $(LOG_SPELL_CHECK).log

pdf.1: $(THESIS_OUTPUT).tex
	@echo -e "\e[0;37m[pandoc  ]\e[0m Building PDF@1..."
	@pdflatex $(PDFLATEX_ARGS) > "$(LOG_BUILD)"_PDF1.log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m PDF@1 build succeeded!" || \
		(echo -e "\e[0;31m[error   ]\e[0m PDF@1 build failed!" >&2 && exit 1)

bib: pdf.1
	@echo -e "\e[0;37m[pandoc  ]\e[0m Building PDF..."
	@biber intermediate > "$(LOG_BUILD)"_BIB.log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m Biber build succeeded!" || \
		(echo -e "\e[0;31m[error   ]\e[0m Biber build failed!" >&2 && exit 1)

pdf: bib
	@echo -e "\e[0;37m[pandoc  ]\e[0m Building PDF@2..."
	@pdflatex $(PDFLATEX_ARGS) > "$(LOG_BUILD)"_PDF2.log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m PDF@2 build succeeded!" || \
		(echo -e "\e[0;31m[error   ]\e[0m PDF@2 build failed!" >&2 && exit 1)
	@echo -e "\e[0;37m[pandoc  ]\e[0m Building PDF@3..."
	@pdflatex $(PDFLATEX_ARGS) > "$(LOG_BUILD)"_PDF3.log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m PDF@3 build succeeded!" || \
		(echo -e "\e[0;31m[error   ]\e[0m PDF@3 build failed!" >&2 && exit 1)
	@mv intermediate.pdf "$(THESIS_OUTPUT).pdf"

tex: $(THESIS_OUTPUT).tex

svg:
	@echo -e "\e[0;34m[info    ]\e[0m Converting svg's to pdf..."
	@find images -type f -name '*.svg' | while read f; do inkscape -z -C -A "$${f%.svg}.pdf" "$$f"; done

cli:
	@touch $(VERSION_CHECK_LOCK)
	@$(MAKE) pdf
	@rm $(VERSION_CHECK_LOCK)

$(VERSION_CHECK_LOCK):
	@echo -e "\e[0;34m[info    ]\e[0m Checking build dependencies..."
	$(call version-check,pandoc,1.17.1)
	$(call version-check,tex,2015)
	$(call version-check,bibtex,2015)
	$(call version-check,latex,2015)

# only needed for view and run target
	$(call version-check,evince,3.8)
	$(call version-check,inotifywait,3.0,--help)
	$(call version-check,hunspell,1.5.3)
	@test -n "$(version_check_failed)" && echo -e "\e[0;31m[error   ]\e[0m Some dependencies are missing!" && exit 1 || true
	@touch "$@"

$(LOG_SPELL_CHECK).log: $(VERSION_CHECK_LOCK) $(THESIS_OUTPUT).tex
	@echo -e "\e[0;37m[hunspell]\e[0m Doing spell check..."
	@hunspell -w -t -m -i utf-8 \
		-d "$(SPELL_CHECK_LANGUAGE)" \
		-p "$(SPELL_CHECK_USER_DICT)" \
		"$(THESIS_OUTPUT)".tex > "$(LOG_SPELL_CHECK)".log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m Spell check succeeded!" || echo -e "\e[0;31m[error   ]\e[0m Spell check failed!" >&2

$(THESIS_OUTPUT).%: $(VERSION_CHECK_LOCK) $(THESIS_FILES) svg
	@echo -e "\e[0;37m[pandoc  ]\e[0m Building $(shell echo $* | tr a-z A-Z)..."
	@pandoc $(call pandoc-args,$*) > "$(LOG_BUILD)"_$*.log 2>&1 && \
		echo -e "\e[0;32m[success ]\e[0m $(shell echo $* | tr a-z A-Z) build succeeded!" || \
		(echo -e "\e[0;31m[error   ]\e[0m $(shell echo $* | tr a-z A-Z) build failed!" >&2 && exit 1)

view: $(VERSION_CHECK_LOCK) $(LOG_SPELL_CHECK).log pdf
	@echo -e "\e[0;34m[info    ]\e[0m Starting PDF viewer..."
	@evince "$(THESIS_OUTPUT)".pdf > /dev/null 2>&1 &

run: $(VERSION_CHECK_LOCK) view
	@while true; do \
		echo -e "\e[0;34m[info    ]\e[0m Waiting for file changes..."; \
		inotifywait -r -e modify,attrib,close_write,move,create,delete \
		--exclude '(Makefile|.*\.log|.*\.lock|$(THESIS_OUTPUT)\..*|\.git|\.gitignore|#.*|.*~|.*\.kate-swp)' \
		--format '%w%f' "." > /dev/null 2>&1; $(MAKE) build; \
	done
